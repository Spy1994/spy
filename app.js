/**
 * Spy Seventh Reconstruction
 *
 * 1、重新规划了目录结构，更加规范合理
 * 2、采用UI-Router进行多视图开发
 * 3、舍弃原有模板样式，重新进行界面设计
 * 4、完全放弃jQuery
 *
 * 2014-9-10 21:36:05 第七次重构
 * 
 * @author Abel i@iue.me
 * @version 0.0.7
 */

var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var mongo        = require('mongoose');
require('./common/validator');

global.utility   = require('utility');
global.option    = require('./common/option');
global.library   = require('./common/library.js');
global.mailer    = require('./common/mailer.js');

// 七牛静态加速服务
var qiniu     = require('node-qiniu');
qiniu.config({'access_key': option.cdnOption.access, 'secret_key': option.cdnOption.secret});
global.bucket = {
  avatar:   qiniu.bucket('spyavatarcdn'),           // 头像服务器
  main:     qiniu.bucket('spycdn'),                 // 主服务器
  album:    qiniu.bucket('spyphotocdn'),            // 相册服务器（*私密）
  resource: qiniu.bucket('spyresourcecdn')          // 其它资源服务器
};

// 初始化应用实例
var app = express();

// 采用Jade作为模板引擎
app.set('views', path.join(__dirname, 'template'));
app.set('view engine', 'jade');

// 连接数据库
var connUrl = 'mongodb://localhost:27017/spy';
global.db = mongo.createConnection(connUrl);

// 中间件
app.use(favicon(path.join(__dirname, '/favicon.ico')));
app.use(express.static(path.join(__dirname, '/bootstrap')));
app.use(express.static(path.join(__dirname, '/bower_components')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser(option.secret));
app.use(session({secret: option.secret, key: 'sid'}));

// 交给路由处理器处理请求
require('./common/dispatcher')(app);

// 监听端口并暴露模块
module.exports = app.listen(3000);