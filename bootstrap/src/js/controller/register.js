/**
 * 注册控制器
 * @author Abel i@iue.me
 */

$spy.controller('register', ['$scope', '$http', 'toastr', '$timeout', 
  function($scope, $http, toastr, $timeout){
  $scope.btnVal = '立即注册';
  $scope.pact = true;

  // 清除错误信息
  $scope.usernameFocus   = function(){ $scope.usernameError = ''; };
  $scope.emailFocus      = function(){ $scope.emailError = ''; };
  $scope.passwordFocus   = function(){ $scope.passwordError = ''; };
  $scope.repasswordFocus = function(){ $scope.repasswordError = ''; };

  // 检测用户名
  $scope.usernameBlur = function(){
    if($scope.username && /^[a-zA-Z]{1,}[0-9a-zA-Z_-]{3,11}$/.test($scope.username)){
      // 检测用户名是否存在
      $http({url: '/action/user/usernameExist', method: 'POST', data: {username: $scope.username}}).success(function(data){
        if(data && data.exist){
          // 已存在
          $scope.usernameError = '用户名已存在';
        }
      });
    }else{
      // 直接报错
      $scope.usernameError = '用户名无效';
    }
  };

  // 检测邮箱
  $scope.emailBlur = function(){
    if($scope.email && /^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/.test($scope.email)){
      // 检测用户名是否存在
      $http({url: '/action/user/emailExist', method: 'POST', data: {email: $scope.email}}).success(function(data){
        if(data && data.exist){
          // 已存在
          $scope.emailError = '邮箱已存在';
        }
      });
    }else{
      // 直接报错
      $scope.emailError = '邮箱无效';
    }
  };

  // 检测密码
  $scope.passwordBlur = function(){
    if(!($scope.password && /^\w{4,16}$/.test($scope.password))){
      $scope.passwordError = '密码无效';
    }
  };

  // 重复密码
  $scope.repasswordBlur = function(){
    if($scope.repassword != $scope.password){
      $scope.repasswordError = '密码不一致';
    }
  };

  // 提交注册
  $scope.registerSubmit = function(){
    if(!$scope.pact){
      alert('您还没有同意用户协议，不能提交注册！');
      return;
    }
    // 判断是否有错误还没有处理
    if($scope.usernameError || $scope.emailError || $scope.passwordError || $scope.repasswordError){
      return;
    }
    // 检测字段
    if(!$scope.username || !$scope.email || !$scope.password || !$scope.repassword){
      if(!$scope.username) $scope.usernameError = '用户名无效';
      if(!$scope.email) $scope.emailError = '邮箱无效';
      if(!$scope.password) $scope.passwordError = '密码无效';
      if(!$scope.repassword) $scope.repasswordError = '密码不一致';
      return;
    }
    $scope.progress = true;
    $scope.btnVal = '正在注册';
    // 提交数据
    $http({method: 'POST', url: '/action/user/register', data:
      {'username': $scope.username, 'password': $scope.password, 'email': $scope.email}})
      .success(function(data){
        $scope.btnVal = '立即注册';
        $scope.progress = false;
        if(data.code == 1){
          $scope.btnVal = '即将跳转';
          $timeout(function(){
            location.href = '/';
          }, 3000);
          toastr.success(data.msg, '注册成功');
        }else{
          switch(data.code){
            case -1: $scope.usernameError = data.msg; break;
            case -2: $scope.emailError = data.msg; break;
            case -3: $scope.passwordError = data.msg; break;
          }
          toastr.error(data.msg, '注册失败');
        }
      });
  };
}]);