/**
 * 用户主页模块
 * @author Abel i@iue.me
 */

$spy.controller('user.index', ['$scope', '$http', '$stateParams', '$timeout', 
  function($scope, $http, $stateParams, $timeout){
    // 对评论框关闭事件进行转发
    $scope.$on('dynamic.closeComment', function(){
      $scope.$broadcast('user.closeComment');
    })
    $scope.retry = $scope.noMore = false;
    $scope.page = -1;
    $scope.timeData = [];
    $scope.loadDynamic = function(){
      $scope.loading = '加载中...';
      if(!$scope.retry) $scope.page ++;
      // 获取动态信息
      $http({method: 'GET', url: '/action/center/dynamic', params: {uid: $stateParams.uid || 0, page: $scope.page}})
      .success(function(data){
        if(data.code > 0){
          if(data.content && data.content.length){
            for(var i in data.content){
              var obj = data.content[i];
              $scope.timeData.push(obj);
            }
            if(data.content.length < 4){
              $scope.noMore = true;
              $scope.loading = '已没有更多';
            }else{
              $scope.loading = '加载更多...';
            }
          }else{
            $scope.noMore = true;
            $scope.loading = '已没有更多';
          }
        }else{
          // 获取失败
          $scope.loading = '获取失败，点击重试';
          $scope.retry = true;
        }
      });
    };
    $scope.blackWall = '热烈庆祝贝壳内测开启~！';
  }
]);