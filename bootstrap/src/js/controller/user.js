/**
 * 用户模块
 * @author Abel i@iue.me
 */

$spy.controller('user', ['$scope', '$stateParams', '$http', 'toastr', 
  function($scope, $stateParams, $http, toastr){
    $http({url: '/action/user/baseInfo', params: $stateParams}).success(function(data){
      if(data.code){
        // 绑定用户数据
        var me                  = data.msg;
        $scope.headerBackground = me.skin.ucenter;
        $scope.userAvatar       = me.avatar;
        $scope.userNickname     = me.nickname;
        $scope.userSex          = me.sex ? true : false;
        $scope.userMaxim        = me.maxim;
        $scope.userArea         = me.area.province + ' ' + me.area.city;
        $scope.userStar         = me.star;
        var graduation          = me.school[me.school.length - 1];
        $scope.userGraduation   = graduation ? graduation.name : '未指定';
        $scope.userGrade        = me.grade.now.alias;
        $scope.userName         = me.username;
        $scope.relation         = me.relation;
        $scope.uid              = me.uid;
        
        if(me.relation == 9) $scope.followShow = false;
        if(me.relation >= 2) $scope.follow = '已关注';
      }else{
        toastr.error(data.msg, '获取失败');
      }
    });

    $scope.followEnter = function(){
      if($scope.relation >= 2){
        $scope.follow = '取消';
      }
    };

    $scope.followLeave = function(){
      if($scope.relation >= 2){
        $scope.follow = '已关注';
      }else{
        $scope.follow = '+ 关注';
      }
    }
}]);