/**
 * 导航条用户窗口
 * @author Abel i@iue.me
 */

$spy.controller('me', ['$scope', '$http', 'toastr', 'ngDialog', function($scope, $http, toastr, ngDialog){
  $scope.load = function(){
    $http({url:'/action/user/baseInfo', method: 'GET'}).success(function(data){
      if(data.code){
        // 绑定用户数据
        var me = data.msg;
        $scope.nickname = me.nickname;
        $scope.grade = me.grade.now.alias;
        $scope.avatar = me.avatar;
        $scope.points = {
          has: me.grade.points,
          upgrade: me.grade.next.points,
          per: Math.floor((me.grade.points - me.grade.now.points) / (me.grade.next.points - me.grade.now.points) * 100)
        };
        $scope.balance = me.balance;
        $scope.points.style = {width: $scope.points.per + '%'};
      }else{
        toastr.error(data.msg, '获取失败');
      }
    });
  };

  $scope.login = function(){
    ngDialog.open({
      template: '/template/dialog/login',
      controller: ['$scope', '$http', function($scope, $http){
        $scope.btnVal = '登录';
        $scope.background = {background: 'url(http://spycdn.qiniudn.com/header.jpg)'};
        $scope.maxim = '你是我的骄傲，曾经。';
        // 登录过程
        $scope.login = function(){
          $scope.process = true;
          $scope.btnVal  = '登录中...';
          $http({method: 'POST', url: '/action/user/login', 
            data: {username: $scope.username, password: $scope.password}}).success(function(data){
              if(data.code){
                // 登录成功，刷新当前页面
                location.reload();
              }else{
                toastr.error(data.msg, '登录失败');
              }
              $scope.process = false;
              $scope.btnVal  = '登录';
            });
        };
      }]
    });
  };
}]);