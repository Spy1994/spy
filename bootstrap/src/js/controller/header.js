/**
 * 头部模块
 * @author Abel i@iue.me
 */

$spy.controller('header', ['$scope', function($scope){
  $scope.title = 'imSpy';
}]);