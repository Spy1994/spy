/**
 * Spy App
 * @author Abel i@iue.me
 */
var $spy = angular.module('spy', ['ui.bootstrap', 'ui.checkbox', 'ngDialog', 'ui.router', 
  'iSpy', 'toastr', 'angular-loading-bar', 'ngAnimate', 'ngSanitize']);

$spy.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 
  function($stateProvider, $urlRouterProvider, $locationProvider){

    $stateProvider.state('index', {
      url: '/'
    })

    .state('reasoning', {

    })

    .state('case', {

    })

    .state('word', {

    })

    .state('group', {

    })

    .state('user', {
      url: '/user/:uid',
      abstract: true,
      views: {
        'main': {
          templateUrl: '/template/user/layout',
          controller: 'user'
        }
      }
    })

    .state('user.index', {
      url: '',
      views: {
        'user': {
          templateUrl: '/template/user/index',
          controller: 'user.index'
        }
      }
    })

    .state('register', {
      url: '/register',
      views: {
        'main': {
          templateUrl: '/template/user/register',
          controller: 'register'
        }
      }
    });

    // html5Mode
    $locationProvider.html5Mode(true).hashPrefix('!');
  }]
);

$spy.filter('nameFilter', function(){
  return function(name){
    return '(@' + name + ')';
  };
});