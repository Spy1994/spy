/**
 * iSpy 交互组件
 * 为了保证组件的统一性，所有名称均以i开头，方便共用
 * @author Abel i@iue.me
 */

(function (window, angular, undefined){
  'use strict';

  var iSpy = angular.module('iSpy', ['ui.bootstrap']);

  /**
   * 拖拽居中组件
   * 对指定声明的元素添加拖动功能，并按需要选择是否自动移动到屏幕中心
   * 注意：由于AngularJS本身原因，在前期对弹窗进行处理时，无法预知弹窗大小，
   *       这会导致部分功能出现异常，所以，组件期望能够获得一个固定的值来避免类似错误
   */
  iSpy.directive('iDrift', ['iClient', function(iClient){
    return {
      restrict: 'EA',
      scope: {iMargin: '@', iWidth: '@', iHeight: '@', iAuto: '@'},
      controller: ['$scope', function($scope){
        $scope.iMarginLeft = $scope.iMarginRight = $scope.iMarginTop = $scope.iMarginBottom = 2;
        $scope.iMove = false;
        // 对Margin进行分析
        if($scope.iMargin){
          var mg = $scope.iMargin.split(' ');
          switch(mg.length){
            case 1:
              $scope.iMarginLeft = $scope.iMarginRight = $scope.iMarginTop = $scope.iMarginBottom = mg[0];
              break;
            case 2:
              $scope.iMarginTop  = $scope.iMarginBottom = mg[0];
              $scope.iMarginLeft = $scope.iMarginRight = mg[1];
              break;
            case 3:
              $scope.iMarginTop    = mg[0];
              $scope.iMarginLeft   = $scope.iMarginRight = mg[1];
              $scope.iMarginBottom = mg[2];
              break;
            case 4:
              $scope.iMarginTop    = mg[0];
              $scope.iMarginRight  = mg[1];
              $scope.iMarginBottom = mg[2];
              $scope.iMarginLeft   = mg[3];
              break;
          }
        }
        // 居中定位
        $scope.autoCenter = function(e, client){
          $scope.$element.css({
            left: (client.width - $scope.iWidth) / 2 + 'px',
            top:  (client.height - $scope.iHeight) / 3 + 'px'
          });
        };
        // 自动适应屏幕的变化
        if($scope.iAuto != 'false'){
          $scope.$on('i.clientResize', $scope.autoCenter);
        }
      }],
      link: function(scope, element){
        scope.$element = element.addClass('i-drift');
        scope.iWidth   = 1 * (scope.iWidth ? scope.iWidth : scope.$element.prop('offsetWidth'));
        scope.iHeight  = 1 * (scope.iHeight ? scope.iHeight : scope.$element.prop('offsetHeight'));
        scope.autoCenter(null, iClient.getVisualsSize());
        // 鼠标按下，开始拖拽
        element.bind('mousedown', function(e){
          var offset = function(){
            var x = 0;
            var y = 0;
            var el = e.target;
            while(el != element[0]){
              x += el.offsetLeft;
              y += el.offsetTop;
              el = el.offsetParent;
            }
            return {x: x, y: y};
          };
          var iOffset = offset();
          scope.iPointX = e.offsetX + iOffset.x;
          scope.iPointY = e.offsetY + iOffset.y;
          scope.iMove = true;
          element.addClass('i-move');
        });
        // 鼠标拖动
        element.bind('mousemove', function(e){
          if(!scope.iMove) return;
          var left = e.x - scope.iPointX;
          var top  = e.y - scope.iPointY;
          if(left < scope.iMarginLeft) left = scope.iMarginLeft;
          if(top  < scope.iMarginTop)  top  = scope.iMarginTop;
          if(iClient.visualWidth  - scope.iMarginRight  < scope.iWidth * 1 + e.x - scope.iPointX) 
            left = iClient.visualWidth - scope.iMarginRight - scope.iWidth;
          if(iClient.visualHeight - scope.iMarginBottom < scope.iWidth * 1 + e.y - scope.iPointY) 
            top = iClient.visualHeight - scope.iMarginBottom - scope.iHeight;
          element.css({
            left: left + 'px',
            top:  top  + 'px'
          });
        });
        // 拖动结束
        element.bind('mouseup', function(){
          scope.iMove = false;
          element.removeClass('i-move');
        });
      }
    };
  }]);

  /**
   * 七牛云储支持
   */
  iSpy.directive('qImage', function(){
    return {
      restrict: 'EA',
      controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs){
        $attrs.$observe('qImage', function(img){
          if(img){
            var scope    = $attrs.qImageScope || 'spycdn';
            var style    = $attrs.qImageStyle || '';
            style = style ? '-' + style : '';
            var file     = $attrs.qImage;
            var imageSrc = 'http://' + $attrs.qImageScope + '.qiniudn.com/' + file + style;
            $attrs.$set('src', imageSrc);
          }
        });
      }],
    };
  });

  iSpy.directive('dynamic', function(){
    return {
      restrict: 'EA',
      replace: true,
      scope: {data: '@'},
      templateUrl: '/template/user/dynamic',
      controller: ['$scope', '$http', '$timeout', 'toastr', function($scope, $http, $timeout, toastr){
        var data = angular.fromJson($scope.data);
        $scope.uid = data.author.uid;
        $scope.nickname = data.author.nickname;
        // 对评论进行处理
        $scope.comments = data.comments;
        $scope.commentCount = $scope.comments.length;
        // 对赞进行处理
        $scope.praise = data.praise;
        $scope.thumbsCount = $scope.praise.length;
        // 判断是否已经赞过
        $scope.isThumb = false;
        for(var i in data.praise){
          if(data.praise[i].author == spyGlobal.sessionUid){
            $scope.isThumb = true;
            break;
          }
        }
        $scope.thumbsTip = $scope.isThumb ? '取消赞' : '赞一下';
        // 内容
        $scope.content = data.content;
        $scope.date = data.date;
        $scope.dateFormat = data.dateFormat;
        $scope.id = data.id;
        $scope.thumbClick = function(id){
          // 判断是取消还是添加
          var url = $scope.isThumb ? '/action/center/cancelDynamicPraise' : '/action/center/addDynamicPraise';
          // 赞
          $http({url: url, method: 'GET', params: {dynamic: $scope.id}})
            .success(function(data){
              if(data.code){
                $scope.isThumb = !$scope.isThumb;
                $scope.thumbsTip = $scope.isThumb ? '取消赞' : '赞一下';
                if($scope.isThumb){
                  $scope.thumbsCount++;
                }else{
                  $scope.thumbsCount--;
                }
              }else{
                toastr.error(data.msg, '操作失败');
              }
            });
        }

        // 评论框展开|这里有一个事件，用于防止多个评论框被同时展开
        $scope.commentFocus = function(){
          $scope.$emit('dynamic.closeComment');
          $scope.addComment = true;
        }
        // 关闭评论框
        $scope.$on('user.closeComment', function(){
          $scope.addComment = false;
        })
      }]
    };
  });

  /**
   * 对需要进行表情处理的内容进行转换
   */
  iSpy.filter('phizContent', function(){
    return function(content){
      return content ? content.replace(/:[a-z0-9]+?:/ig, function(face){
        var _face = face.slice(1).slice(0, -1);
        return EMOJI.indexOf(_face) >= 0 ? '<span class="i-face emoji s_' + _face + '"></span>' : face;
      }) : '';
    }
  });

  /**
   * 长度过滤器
   */
  iSpy.filter('length', function(){
    return function(content){
      return content ? content.length : 0;
    }
  });

  /**
   * 对全局昵称进行统一，便于以后扩展
   */
  iSpy.filter('nickname', function(){
    return function(nickname){
      return nickname;
    }
  })

  /**
   * 客户端模块
   * 可以方便快捷的获取一些客户端信息
   */
  iSpy.factory('iClient', ['$rootScope', '$window', function ($rootScope, $window){
    var Client = {
      visualWidth: 0,
      visualHeight: 0,
      getVisualsSize: function(){
        this.visualWidth  = document.documentElement.clientWidth;
        this.visualHeight = document.documentElement.clientHeight;
        return {
          width:  this.visualWidth,
          height: this.visualHeight,
        };
      }
    };

    angular.element($window).bind('resize', function(){
      $rootScope.$broadcast('i.clientResize', Client.getVisualsSize());
    });
    return Client;
  }]);

})(window, window.angular);