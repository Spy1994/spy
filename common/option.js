/**
 * Spy站点静态配置
 * @author Abel i@iue.me
 */

module.exports = {
  // 公共配置
  name: 'imSpy',                            // 站点名称
  secret: 'Spy && Ly',                      // 签名秘钥
  expires: 24 * 60 * 60 * 30 * 1000,        // 会话有效期
  // 用户中心
  center: {
    visitExpires: 60 * 60 * 1000 * 2,       // 访客访问时长（2小时记录一次访问）
    albumLock: 'lock.jpg',                  // 相册锁定图片
  },
  // 邮箱服务配置
  mailer: {
    host: 'smtp.exmail.qq.com',             // 服务器主机
    port: 465,                              // 服务端口
    secure: true,                           // 使用SSL
    auth: {                                 
      user: 'i@iue.me',                     // 用户名
      pass: 'xiedong1994'                   // 密码
    }
  },
  // 静态加速服务器列表
  cdn: {
    // 头像服务器地址
    avatar: {
      encrypt: false,
      name: 'spyavatarcdn',
      url: 'http://spyavatarcdn.qiniudn.com/' 
    },
    // 主服务器地址
    main: {
      encrypt: false,
      name: 'spycdn',
      url: 'http://spycdn.qiniudn.com/',
    },
    // 资源服务器地址
    resource:{
      encrypt: false,
      name: 'spyresourcecdn',
      url: 'http://spyresourcecdn.qiniudn.com/'
    },
    // 相册服务器地址
    album: {
      encrypt: true,
      name: 'spyphotocdn',
      url: 'http://spyphotocdn.qiniudn.com/'
    }
  },
  // 静态加速服务器配置 
  cdnOption: {
    access: '1BA9L0sReqJa2AL-EwKvArMdK7nifdK6u9B3gveH',
    secret: 'ESgLHFP3oCBi58Tf3rQdYUk9cXQHEk9AyL7PCsKo',
    expires: 60 * 60 * 2 * 1000,                     // 每次请求有效期（2小时）
  }
};