/**
 * 邮件服务接口
 * @author Abel i@iue.me
 */
var mailer        = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var validator     = require('validator');
var util          = require('util');

var transport = mailer.createTransport(smtpTransport(option.mailer));

module.exports = mail = {};

/**
 * 发送邮件
 * @param  {String} recipient 收件人
 * @param  {String} content   内容
 * @param  {String} subject   主题
 */
mail.sendMail = function(recipient, content, subject){
  subject = subject || util.format('%s的邮件', option.name);
  var sender = util.format('%s <%s>', option.name, option.mailer.auth.user);
  transport.sendMail({from: sender, to: recipient, subject: subject, html: content});
};