/**
 * 自定义验证库|扩展Validator
 * @author Abel i@iue.me
 */
var validator = require('validator');

module.exports = {};

/**
 * 验证是否是一个合法的用户名
 * @param  {String} username 用户名|4-12位，小写字母开头，数字、下划线、大小写字母
 * @return {Boolean}
 */
validator.extend('isUsername', function(username){
  return /^[a-z]{1}[a-zA-Z0-9_]{3,11}/.test(username);
});

/**
 * 验证是否是一个合法的密码
 * @param  {String} password 密码|6-16位，任意字符组合
 * @return {Boolean}
 */
validator.extend('isPassword', function(password){
  return /^\w{4,16}/.test(password);
});