/**
 * 公共方法库
 * @author Abel i@iue.me
 */
var crypto    = require('crypto');

module.exports = library = {};

/**
 * 获取客户端IP地址
 * @param {Request} req 请求对象
 * @return {String}
 */
library.getClientIP = function(req){
  return req.headers['x-forwarded-for'] || req.connection.remoteAddress || 
         req.socket.remoteAddress || req.connection.socket.remoteAddress;
};

/**
 * 根据当前时间戳生成唯一token
 * @return {String}
 */
library.getToken = function(){ return library.encrypt(Date.now() * Math.random()); };

/**
 * 加密，不可逆
 * @param  {Object} str 欲加密的对象
 * @return {String}     返回加密好的字符串
 */
library.encrypt = function(str){
  if(typeof str == 'undefined') return '';
  str = crypto.createHash('sha1').update(str.toString()).digest('hex');
  return crypto.createHash('md5').update(str.slice(10, 30)).digest('hex');
};

/**
 * 经验/等级字典
 * @type {Array}
 */
library.map = [
  {points:0, alias:'警员(PC)'},
  {points:100, alias:'高级警员(SPC)'},
  {points:300, alias:'警长(SGT)'},
  {points:600, alias:'警署警长(SSGT)'},
  {points:1000, alias:'见习督查(PI)'},
  {points:1500, alias:'督查(IP)'},
  {points:2100, alias:'高级督查(SIP)'},
  {points:2800, alias:'总督查(CIP)'},
  {points:3600, alias:'警司(SP)'},
  {points:4500, alias:'高级警司(SSP)'},
  {points:5500, alias:'总警司(CSP)'},
  {points:6600, alias:'助理警务处长(ACP)'},
  {points:7800, alias:'高助警务处长(SACP)'},
  {points:9100, alias:'警务处副处长(DCP)'},
  {points:10500, alias:'警务处处长(CP)'}
];

/**
 * 根据当前积分返回积分信息数组
 * @param  {Number} points 当前积分
 * @return {Array}         返回信息数组
 */
library.getPoints = function(points){
  var map = this.map;
  var next = map[0];
  for(var n in map) if(map[n].points > points){
    next = map[n];
    break;
  }
  var now  = map[0];
  for(var i in map) if(map[i].points > points){
    now = map[i - 1] || map[0];
    break;
  }
  return {next:next, now: now, points: points};
};

/**
 * 时间格式化
 * @param  {Number}  date     时间
 * @param  {String}  format   格式(留空将友好化输出)
 * @return {String}           
 */
library.dateFormat = function(date, format){
  if(format && format.length > 0) return date.toFormat(format);
  var compare = (new Date().getTime() - new Date(date).getTime()) / 1000;
  // 计算年
  if(Math.floor(compare / (60 * 60 * 24 * 356))) 
    return Math.floor(compare / (60 * 60 * 24 * 356)) + ' 年前';
  if(Math.floor(compare / (60 * 60 * 24 * 30))) 
    return Math.floor(compare / (60 * 60 * 24 * 30)) + ' 月前';
  if(Math.floor(compare / (60 * 60 * 24))) 
    return Math.floor(compare / (60 * 60 * 24)) + ' 天前';
  if(Math.floor(compare / (60 * 60))) 
    return Math.floor(compare / (60 * 60)) + ' 小时前';
  if(Math.floor(compare / (60))) 
    return Math.floor(compare / (60)) + ' 分钟前';
  if(Math.floor(compare)) 
    return Math.floor(compare) + ' 秒前';
  return '刚刚';
};

/**
 * 获取默认skin信息
 * @return {Object} 默认Skin数据
 */
library.getDefaultSkin = function(){
  return {ucenter: 'center-default.jpg', card: 'card-default.jpg'};
};