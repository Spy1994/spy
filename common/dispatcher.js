/**
 * 路由调度器
 * 
 * 所有除静态文件的请求都会经过这里进行处理，并且具体的
 * 处理由control文件夹下的相应模块来完成，所以，如果未
 * 特别指明以下文件以及文件将相对于control作为根目录
 * 
 * 特别的，对于Angualr的Html5Mode，需要对路由进行转发
 * 所以，为了避免与主模块的冲突，规定了以下请求命名方式
 * 
 * action:   动作，表示一个常规请求
 * template: 视图，请求一个模板
 * api:      接口，通过提交的数据请求一个数据
 * 
 * @author Abel i@iue.me
 */
var userModel = require('../model/user');

var CONTROL  = '../control';
var ACTION   = CONTROL + '/action';
var TEMPLATE = CONTROL + '/template';
var API      = CONTROL + '/api';


module.exports = function(app){
  
  // 站点请求前置处理，用于重启会话
  app.use(function(req, res, next){
    if(!req.session.uid && req.signedCookies.token && req.signedCookies.token.length == 32){
      // 尝试获取用户信息
      userModel.findOne({'session.token': req.signedCookies.token}, {uid: true, session: true}, function(err, user){
        // 判断会话数据是否在有效期之内
        if(user && user.session && user.session.date && (user.session.date > (Date.now() - option.expires))){
          userModel.updateSession(user.uid, req, res);
          req.session.uid = user.uid;
          next();
        }else{
          // 删除Cookie，避免下次请求任然进行无效验证
          res.clearCookie('token');
          next();
        }
      });
    }else{
      // 执行下一个中间件
      next();
    }
  });

  /**
   * 绑定基础数据
   */
  app.use(function(req, res, next){
    // req.session.uid = 9;
    res.locals.session = req.session;
    next();
  });

  // 站点首页，事实上，对于单页站点来说，这就是整个网站的中枢
  app.get('/', function(req, res){ res.render('common/layout'); });

  // 用户模块
  app.use('/action/user', require(ACTION + '/user.js'));

  // 用户中心
  app.use('/action/center', require(ACTION + '/center.js'));

  // 用户模板
  app.use('/template/user', require(TEMPLATE + '/user'));

  // 弹窗模板
  app.use('/template/dialog', require(TEMPLATE + '/dialog'));

  // 地址转发
  app.use(function(req, res){
    var path = ['reasoning', 'case', 'user', 'word', 'group', 'register'];
    if(-1 != path.indexOf(req.url.substr(1).split('/')[0])){
      res.render('common/layout');
    }else{
      res.end('404');
    }
  });
};