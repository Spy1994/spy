/**
 * 动态模型
 * @author Abel i@iue.me
 */

var Schema  = require('mongoose').Schema;
var autoInc = require('mongoose-auto-increment');
var userModel = require('./user');

// 初始化自动增长(全局DB对象)
autoInc.initialize(db);

var dynamicSchema = new Schema({
  id:         {type: Number, unique: true, index: true}, // 动态ID
  author:     {type: Number},                            // 作者ID
  date:       {type: Number, default: Date.now()},       // 发布时间
  comments:   {type: Array, default: []},                // 评论数组
  praise:     {type: Array, default: []},                // 赞数组，储存用户基础信息
  type:       {type: Number, default: 0},                // 动态类型
  content:    {type: Object, default: {}},               // 内容|根据不同类型储存不同数据，最鸡冻人心的部分
  state:      {type: Number, default: 1},                // 状态码|0、删除，1、正常
}, {collection: 'dynamic'});

/**
 * 发送一条普通动态
 * @param  {Number}   uid      用户ID
 * @param  {String}   content  发布的内容
 * @param  {Function} callback 回调函数
 */
dynamicSchema.static('sendNormalDynamic', function(uid, content, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(err){};
  this.create({author: uid, content: content}, callback);
});

/**
 * 回复动态
 * @param  {Number}   did      动态ID
 * @param  {Number}   pid      评论父级ID
 * @param  {Number}   uid      发送评论的用户
 * @param  {String}   content  评论内容
 * @param  {Function} callback 回调函数
 */
dynamicSchema.static('replyDynamic', function(did, pid, uid, content, callback){
  var me = this;
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  userModel.findOne({uid: uid, state: 1}, {_id: false, uid: true, name: true, nickname: true}, function(err, user){
    if(user && user.uid){
      // 评论信息
      var comment = {
        hash: Math.floor(Math.random() * 10000) + 1,                  // 避免重复|+1是为了避免产生0，提高判断工作复杂程度
        author: user.uid,                                             // 用户ID 
        parent: pid,                                                  // 评论父级ID|时间和hash的结合体
        date: Date.now(),                                             // 评论时间
        content: content,                                             // 评论内容
        nickname: user.nickname || user.name                          // 评论者的昵称
      };
      me.update({id: did}, {$push:{comments: comment}}, function(err, row){
        if(row){
          callback({code: 1, msg: '回复成功'});
        }else{
          callback({code: 0, msg: '回复失败'});
        }
      });
    }else{
      callback({code: 0, msg: '用户不存在或用户名无效'});
    }
  });
});

/**
 * 删除动态
 * @param  {Number}   did      动态ID
 * @param  {Number}   uid      进行操作的用户ID
 * @param  {Function} callback 回调函数
 */
dynamicSchema.static('removeDynamic', function(did, uid, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  this.update({id: did, author: uid, state: 1}, {$set: {state: 0}}, function(err, row){
    if(row){
      callback({code: 1, msg: '已删除'});
    }else{
      callback({code: 0, msg: '删除动态失败'});
    }
  });
});

/**
 * 删除评论|发布动态的用户和发布评论的用户都可以删除评论
 * @param  {Number}   did      动态ID
 * @param  {Number}   uid      用户ID
 * @param  {Number}   date     评论日期
 * @param  {Number}   hash     评论HASH
 * @param  {Function} callback 回调函数
 */
dynamicSchema.static('removeComment', function(did, uid, date, hash, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  this.update({id: did, state: 1, $or: [{comments: {$elemMatch: {author: uid}}}, {author: uid}]},
    {$pull: {comments: {date: date, hash: hash}}}, function(err, row){
      if(row){
        callback({code: 1, msg: '已删除'});
      }else{
        callback({code: 0, msg: '删除评论失败'});
      }
    }
  );
});

/**
 * 赞一下动态
 * @param  {Number}   did      动态ID
 * @param  {Number}   uid      操作用户
 * @param  {Function} callback 回调函数
 */
dynamicSchema.static('praiseDynamic', function(did, uid, callback){
  var me = this;
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  userModel.findOne({uid: uid, state: 1}, {_id: false, uid: true, name: true, nickname: true}, function(err, user){
    if(user && user.uid){
      // 判断是否已经赞过
      me.findOne({id: did, 'praise.author': uid}, function(err, dynamic){
        if(dynamic && dynamic.id){
          // 已经存在
          callback({code: 0, msg: '您已经赞过啦~！'});
        }else{
          // 准备数据
          var praise = {
            author: user.uid,                                         // 用户ID
            nickname: user.nickname || user.name,                     // 昵称
            date: Date.now()                                          // 时间
          };
          me.update({id: did, state: 1}, {$push:{praise: praise}}, function(err, row){
            if(row){
              callback({code: 1, msg: '已赞！'});
            }else{
              callback({code: 0, msg: '赞失败'});
            }
          });
        }
      });
    }else{
      callback({code: 0, msg: '用户不存在或用户已被禁用'});
    }
  });
});

/**
 * 取消赞
 * @param  {Number}   did      动态ID
 * @param  {Number}   uid      用户ID
 * @param  {Function} callback 回调函数
 */
dynamicSchema.static('cancelPraise', function(did, uid, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  this.update({id: did, state: 1},
    {$pull: {praise: {author: uid}}}, function(err, row){
      if(row){
        callback({code: 1, msg: '已取消'});
      }else{
        callback({code: 0, msg: '失败!'});
      }
    }
  );
});

// 设置自增ID
dynamicSchema.plugin(autoInc.plugin, {model: 'dynamic', field: 'id', startAt: 1});

module.exports = db.model('dynamic', dynamicSchema);