/**
 * 个人中心模型
 * @author Abel i@iue.me
 */

var Schema    = require('mongoose').Schema;
var userModel = require('./user');

var centerSchema = new Schema({
  uid:    {type: Number, unique: true, index: true}, // 动态ID
  note:   {type: String, deafult: ''},               // 小黑板
  visits: {type: Array, default: []},                // 访客列表
  skin:   {type: Object, default: {}},               // 皮肤数据
}, {collection: 'center'});

/**
 * 初始化
 * @param  {Number} uid 用户ID
 */
centerSchema.static('init', function(uid){
  this.create({uid: uid}, function(err){});
});

/**
 * 记录用户访问
 * @param  {Number}   uid      原用户
 * @param  {Number}   sid      操作用户
 * @param  {Function} callback 回调函数
 */
centerSchema.static('recordVisit', function(uid, sid, callback){
  var me = this;
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  userModel.findOne({uid: sid, state: 1}, {_id: false, uid: true, name: true, nickname: true}, function(err, user){
    if(user && user.uid){
      // 判断一定时间内是否访问过，访问过则不记录
      me.findOne({uid: uid, visits: {$elemMatch: {date: {$gt: Date.now() - option.center.visitExpires}, author: sid}}}, 
        {_id: false, uid: true}, function(err, center){
          if(center && center.uid){
            // 一定时间内已经访问过了，不重复记录
            callback({code: 1, msg: ';)'});
          }else{
            // 准备数据
            var visit = {
              author: user.uid,                                         // 用户ID
              avatar: user.avatar,                                      // 头像信息
              nickname: user.name || user.nickname,                     // 昵称
              date: Date.now()                                          // 时间
            };
            me.update({uid: uid}, {$push:{visits: visit}}, function(err, row){
              if(row){
                callback({code: 1, msg: ';)'});
              }else{
                callback({code: 0, msg: ':('});
              }
            });
          }
        }
      );
    }else{
      callback({code: 0, msg: '用户不存在或用户名无效'});
    }
  });
});

module.exports = db.model('center', centerSchema);