/**
 * 相册模型
 * @author Abel i@iue.me
 */

var Schema    = require('mongoose').Schema;
var autoInc   = require('mongoose-auto-increment');
var userModel = require('./user');


// 初始化自动增长(全局DB对象)
autoInc.initialize(db);

var albumSchema = new Schema({
  id:       {type: Number, unique: true, index: true}, // 相册ID
  name:     {type: String, required: true},            // 相册名
  info:     {type:String, default: ''},                // 相册描述
  cover:    {type: String, default: ''},               // 相册封面
  owner:    {type: Number},                            // 相册所有者
  date:     {type: Number, default: Date.now()},       // 创建时间
  comments: {type: Array, default: []},                // 评论数组
  praise:   {type: Array, default: []},                // 赞数组，储存用户基础信息
  type:     {type: Number, default: 0},                // 相册类型|0、普通，1、加密，2、私有
  photos:   {type: Array, default: []},                // 相册数组
  visits:   {type: Array, default: []},                // 浏览过的人
  encrypt:  {type: Object, default: {}},               // 加密信息
  state:    {type: Number, default: 1},                // 状态码|0、删除，1、正常
}, {collection: 'album'});

/**
 * 评论相册
 * @param  {Number}   aid      相册ID
 * @param  {Number}   pid      评论父级ID
 * @param  {Number}   uid      发送评论的用户
 * @param  {String}   content  评论内容
 * @param  {Function} callback 回调函数
 */
albumSchema.static('replyAlbum', function(aid, pid, uid, content, callback){
  var me = this;
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  userModel.findOne({uid: uid, state: 1}, {_id: false, uid: true, name: true, nickname: true}, function(err, user){
    if(user && user.uid){
      // 评论信息
      var comment = {
        hash: Math.floor(Math.random() * 10000) + 1,                  // 避免重复|+1是为了避免产生0，提高判断工作复杂程度
        author: user.uid,                                             // 用户ID 
        parent: pid,                                                  // 评论父级ID
        date: Date.now(),                                             // 评论时间
        content: content,                                             // 评论内容
        avatar: user.avatar,                                          // 评论者的头像
        nickname: user.name || user.nickname                          // 评论者的昵称
      };
      me.update({id: aid}, {$push:{comments: comment}}, function(err, row){
        if(row){
          callback({code: 1, msg: '回复成功'});
        }else{
          callback({code: 0, msg: '回复失败'});
        }
      });
    }else{
      callback({code: 0, msg: '用户不存在或用户名无效'});
    }
  });
});

/**
 * 删除相册
 * @param  {Number}   aid      相册ID
 * @param  {Number}   uid      进行操作的用户ID
 * @param  {Function} callback 回调函数
 */
albumSchema.static('removeAlbum', function(aid, uid, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  this.update({id: aid, owner: uid, state: 1}, {$set: {state: 0}}, function(err, row){
    if(row){
      callback({code: 1, msg: '已删除'});
    }else{
      callback({code: 0, msg: '删除相册失败'});
    }
  });
});

/**
 * 获取某个用户的相册列表
 * @param  {Number}   uid      用户ID
 * @param  {Number}   sid      请求用户
 * @param  {Function} callback 回调函数
 */
albumSchema.static('getList', function(uid, sid, callback){
  callback   = ('function' == (typeof callback)) ? callback : function(result){};
  var where  = {owner: uid, state: 1, $or: [{type: 0}, {type: 1}]};
  var isSelf = false;
  if(uid == sid){
    where.owner = sid;
    isSelf = true;
  }
  this.find(where, {_id: false, id: true, name: true, cover: true, owner: true, type: true}, function(err, albums){
    if(albums){
      var box = [];
      // 对结果进行处理
      for(var i in albums){
        var album = albums[i];
        // 加密状态，将封面换为加密图片
        if(album.type == 1 && !isSelf) album.cover = bucket.main.key(option.center.albumLock).url();
        box.push(album);
      }
      callback({code: 0, msg: box});
    }else{
      callback({code: 0, msg: '获取相册列表失败'});
    }
  });
});

/**
 * 访问相册
 * @param  {Number}   id       相册ID
 * @param  {Number}   type     相册类型|0、普通，1、加密，2、私有
 * @param  {Number}   sid      请求用户
 * @param  {String}   answer   答案|可留空
 * @param  {Function} callback 回调函数
 */
albumSchema.static('visitAlbum', function(id, type, sid, answer, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  where = {id: id, type: type};
  // 如果类型是加密相册，增加回答，答案不能为空
  if(type == 1){
    if(!answer){
      callback({code: 0, msg: '答案不能为空'});
      return;
    }
    where['encrypt.answer'] = answer;
  }else if(type == 2){
    // 私有相册，只允许所有者访问
    where.owner = sid;
  }
  this.findOne(where, function(err, album){
    if(album && album.id){
      // 判断是否是黑名单用户，如果是即便答案通过也无法进行访问
      userModel.isBlack(album.owner, sid, function(is){
        if(is){
          // 返回无权限
          callback({code: 0, msg: '很抱歉，您没有权限访问此相册'});
        }else{
          // 构建相册数据
          var photos = [];
          for(var i in album.photos){
            var photo = album.photos[i];
            photo.url = bucket.ablum.key(album.photos[i].key).fop().imageView({mode: 2, height: 120, width: 200}).token().url;
            photos.push(photo);
          }
          callback({code: 1, msg: photos});
        }
      });
    }else{
      callback({code: 0, msg: type == 1 ? '密码有误' : '指定相册不存在'});
    }
  });
});

/**
 * 赞
 * @param  {Number}   aid      相册ID
 * @param  {Number}   uid      操作用户
 * @param  {Function} callback 回调函数
 */
albumSchema.static('praiseAlbum', function(aid, uid, callback){
  var me = this;
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  userModel.findOne({uid: uid, state: 1}, {_id: false, uid: true, name: true, nickname: true}, function(err, user){
    if(user && user.uid){
      // 判断是否已经赞过
      me.findOne({id: did, 'praise.author': uid}, function(err, album){
        if(album && album.id){
          // 已经存在
          callback({code: 0, msg: '您已经赞过啦~！'});
        }else{
          // 准备数据
          var praise = {
            author: user.uid,                                         // 用户ID
            avatar: user.avatar,                                      // 头像信息
            nickname: user.name || user.nickname,                     // 昵称
            date: Date.now()                                          // 时间
          };
          me.update({id: did, state: 1}, {$push:{praise: praise}}, function(err, row){
            if(row){
              callback({code: 1, msg: '已赞'});
            }else{
              callback({code: 0, msg: '赞失败'});
            }
          });
        }
      });
    }else{
      callback({code: 0, msg: '用户不存在或用户已被禁用'});
    }
  });
});

/**
 * 取消赞
 * @param  {Number}   aid      相册ID
 * @param  {Number}   uid      用户ID
 * @param  {Function} callback 回调函数
 */
albumSchema.static('cancelPraise', function(aid, uid, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(result){};
  this.update({id: aid, state: 1},
    {$pull: {praise: {author: uid}}}, function(err, row){
      if(row){
        callback({code: 1, msg: '已取消赞'});
      }else{
        callback({code: 0, msg: '取消赞失败'});
      }
    }
  );
});


// 设置自增ID
albumSchema.plugin(autoInc.plugin, {model: 'album', field: 'id', startAt: 1});

module.exports = db.model('album', albumSchema);