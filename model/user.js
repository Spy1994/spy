/**
 * 用户模型
 * @author Abel i@iue.me
 */

var Schema  = require('mongoose').Schema;
var autoInc = require('mongoose-auto-increment');

// 初始化自动增长(全局DB对象)
autoInc.initialize(db);

var defaultArea = {country: '中国', province: '北京', city: '东城区'};

// 用户模型
var userSchema = new Schema({
  uid:           {type: Number, unique: true, index: true},             // 用户ID
  name:          {type: String, unique: true},                          // 用户名
  pass:          {type: String, required: true, length: 32},            // 密码|定长32位
  mail:          {type: String, unique: true, lowercase: true},         // 邮箱|自动转换为小写
  register:      {type: Object, required: true},                        // 注册信息
  login:         {type: Object, default: {}},                           // 登录信息
  nickname:      {type: String, default: ''},                           // 昵称
  maxim:         {type: String, default: ''},                           // 格言
  area:          {type: Object, default: defaultArea},                  // 地区|{country: 86, area: 110001}
  fans:          {type: Array, default: []},                            // 粉丝数组|只存放用户ID
  idol:          {type: Array, default: []},                            // 关注数组|只存放用户ID
  black:         {type: Array, default: []},                            // 黑名单用户|只存放用户ID
  message:       {type: Array, default: []},                            // 邮件信息|存放邮件ID相对于用户的状态{msg: 1, state: 1}
  sex:           {type: Number, default: 0},                            // 性别
  age:           {type: Number, default: 0},                            // 年龄
  birthday:      {type: Number, default: Date.now()},                   // 生日
  constellation: {type: String, default: '白羊座'},                     // 星座
  balance:       {type: Number, default: 0},                            // 账户余额
  points:        {type: Number, default: 0},                            // 积分
  school:        {type: Array, default: []},                            // 学校
  state:         {type: Number, default: 1},                            // 用户状态|0、状态异常，1、正常，-1、禁止登录
  session:       {type: Object, default: {}},                           // 会话信息
  skin:          {type: Object, default: null},                         // 皮肤数据
}, {collection: 'user'});

// 自动增长字段
userSchema.plugin(autoInc.plugin, {model:'user', field:'uid', startAt: 1});

/**
 * 更新用户会话信息
 * @param  {Number}   uid      用户ID
 * @param  {Request}  req      请求信息
 * @param  {Response} res      响应信息
 * @param  {Function} callback 回调函数
 * @return {String}            返回token
 */
userSchema.static('updateSession', function(uid, req, res, callback){
  var ip    = library.getClientIP(req);                           // 获取客户端IP地址
  var agent = req.get('User-Agent');                              // 获取客户端环境信息
  var token = library.getToken();                                 // 生成令牌
  callback  = ('function' == (typeof callback)) ? callback : function(err){};
  // 准备数据对象
  var sessionData = {token: token, date: Date.now(), ip: ip};
  var loginData   = {ip: ip, date: Date.now(), agent: agent};
  this.update({uid: uid}, {$set: {session: sessionData, login: loginData}}, {upsert: true}, callback);
  // 设置cookie信息
  res.cookie('token', token, {maxAge: option.expires, signed: true});
  return token;
});

/**
 * 判断是否是某个用户的黑名单用户
 * @param  {Number}   uid      原用户ID
 * @param  {Number}   sid      请求的用户
 * @param  {Function} callback 回调函数
 */
userSchema.static('isBlack', function(uid, sid, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(err){};
  this.findOne({uid: uid}, {_id: false, black: true}, function(err, user){
    if(user && user.black){
      callback(user.black.indexOf(sid) < 0 ? true : false);
    }else{
      callback(false);
    }
  });
});

/**
 * 根据用户数组获取用户名称数组
 * @param  {Array}    uidArray 用户数组
 * @param  {Function} callback 毁掉函数
 */
userSchema.static('getTeamNames', function(uidArray, callback){
  callback  = ('function' == (typeof callback)) ? callback : function(err){};
  // 对UID进行处理
  var buffer = [];
  for(var i in uidArray){
    buffer.push({uid: uidArray[i]});
  }
  this.find({$or: buffer}, {_id: false, uid: true, nickname: true, name: true, age: true}, callback);
});

module.exports = db.model('user', userSchema);