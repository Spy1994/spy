/**
 * 个人中心
 * @author Abel i@iue.me
 */
var router       = require('express').Router();
var albumModel   = require('../../model/album');
var dynamicModel = require('../../model/dynamic');
var centerModel  = require('../../model/center');
var userModel    = require('../../model/user');

// 前置方法
router.use(function(req, res, next){
  var sid = req.session.uid;
  var uid = req.query.uid;
  if(sid == uid){
    next();
    return;
  }
  var visits = (req.signedCookies.centerVisits || '').slice(',');
  if(typeof visits == 'object' && visits.indexOf(sid) >= 0) next();
  else{
    // 记录访问信息
    centerModel.recordVisit(uid, sid, function(result){
      if(result.code){
        // 记录Cookie，能避免绝大多数情况下的查库
        res.cookie('centerVisits', {maxAge: option.center.visitExpires, signed: true});
      }
      next();
    });
  }
});

// 赞
router.get('/removeAlbum', function(req, res){
  var album = req.query.album;                  // 动态ID
  var sid   = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!album){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  albumModel.removeAlbum(album, sid, function(result){
    res.end(JSON.stringify(result));
  });
});

// 删除相册
router.get('/removeAlbum', function(req, res){
  var album = req.query.album;                  // 动态ID
  var sid   = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!album){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  albumModel.removeAlbum(album, sid, function(result){
    res.end(JSON.stringify(result));
  });
});

// 评论相册
router.post('/replyAlbum', function(req, res){
  var album   = req.body.album;
  var pid     = req.body.pid || 0;
  var content = req.body.content;
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!(album && content)){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  albumModel.replyAlbum(album, pid, sid, content, function(result){
    res.end(JSON.stringify(result));
  });
});

// 创建相册
router.post('/createAlbum', function(req, res){
  var name = req.body.name;
  var info = req.body.info;
  var sid = req.session.uid;
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!name){
    res.end(JSON.stringify({code: 0, msg: '相册名称不能为空'}));
    return;
  }
  albumModel.create({name: name, owner: sid}, function(err, album){
    if(album && album.id){
      res.end(JSON.stringify({code: 1, msg: album.id}));
    }else{
      res.end(JSON.stringify({code: 0, msg: '创建失败，请重试！'}));
    }
  });
});

// 获取相册列表
router.get('/getAlbum', function(req, res){
  var uid = req.query.uid;
  var sid = req.session.uid;
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!uid){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  albumModel.getList(uid, sid, function(result){
    res.end(JSON.stringify(result));
  });
});

// 获取相册内照片的访问权限
router.get('/getAlbumPhotos', function(req, res){
  var album  = req.query.album;
  var type   = req.query.type;
  var sid    = req.session.uid || 0;
  var answer = req.query.answer;
  albumModel.visitAlbum(album, type, sid, answer, function(result){
    res.end(JSON.stringify(result));
  });
});

// 编辑小黑板
router.post('/editNote', function(req, res){
  var content = req.body.content;
  var sid     = req.session.uid;
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!content){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  centerModel.update({uid: sid}, {note: content}, function(err, row){
    var result = {};
    if(row){
      result = {code: 1, msg: '修改成功'};
    }else{
      result = {code: 0, msg: '修改失败'};
    }
    res.end(JSON.stringify(result));
  });
});

// 取消赞
router.get('/cancelDynamicPraise', function(req, res){
  var dynamic = req.query.dynamic;                  // 动态ID
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!dynamic){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  dynamicModel.cancelPraise(dynamic, sid, function(result){
    res.end(JSON.stringify(result));
  });
});

// 赞
router.get('/addDynamicPraise', function(req, res){
  var dynamic = req.query.dynamic;                  // 动态ID
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!dynamic){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  dynamicModel.praiseDynamic(dynamic, sid, function(result){
    res.end(JSON.stringify(result));
  });
});

// 删除动态中的评论
router.get('/removeDynamicComment', function(req, res){
  var dynamic = req.query.dynamic;                  // 动态ID
  var date    = req.query.date;                     // 评论创建的时间
  var hash    = req.query.hash;                     // 随机HASH
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!(dynamic && date && hash)){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  dynamicModel.removeComment(dynamic, sid, date, hash, function(result){
    res.end(JSON.stringify(result));
  });
});

// 回复动态
router.post('/replyDynamic', function(req, res){
  var dynamic = req.body.dynamic;
  var pid     = req.body.pid || 0;
  var content = req.body.content;
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  sid = 2;
  if(!(dynamic && content)){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  dynamicModel.replyDynamic(dynamic, pid, sid, content, function(result){
    res.end(JSON.stringify(result));
  });
});

// 删除动态
router.get('/removeDynamic', function(req, res){
  var dynamic = req.query.dynamic;                  // 动态ID
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  if(!dynamic){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  dynamicModel.removeDynamic(dynamic, sid, function(result){
    res.end(JSON.stringify(result));
  });
});

// 获取动态
router.get('/dynamic', function(req, res){
  var uid = req.query.uid == 0 ? req.session.uid : req.query.uid;
  if(!uid){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  // 分页|考虑性能和分页的准确性
  // 如果上一个列表中有一项被删除了，第二次获取的数据会向后移动一位，导致中间有一条数据无法得到显示
  // 另外，id偏移时，效率也明显比skip高很多
  // 所以采用ID偏移来做分页
  var id     = req.query.id || 0;
  var length = 10;
  // 获取动态
  dynamicModel.find()
    .select({_id: false, id: true, author: true, content: true, type: true, praise: true, comments: true, date: true})
    .where({author: uid, state: 1, id: {$gt: id}}).limit(length).exec(function(err, dynamics){
      // 这里不能判断dynamics，因为空数据一样会引起false
      if(err){
        res.end(JSON.stringify({code: 0, msg: '获取失败，请重试'}));
      }else{
        var data     = [];
        var uidArray = [];
        for(var i in dynamics){
          uidArray.push(dynamics[i].author);
          var buffer = {comments: [], praise: []};
          for(var comment in dynamics[i].comments){
            var tmp = dynamics[i].comments[comment];
            tmp.dateFormat = library.dateFormat(tmp.date);
            buffer.comments.push(tmp);
          }
          for(var item in dynamics[i].praise){
            var tmp = dynamics[i].praise[item];
            tmp.dateFormat = library.dateFormat(tmp.date);
            buffer.praise.push(tmp);
          }
          data.push({
            author:     dynamics[i].author,
            comments:   dynamics[i].comments,
            content:    dynamics[i].content,
            date:       dynamics[i].date,
            id:         dynamics[i].id,
            praise:     dynamics[i].praise,
            dateFormat: library.dateFormat(dynamics[i].date)
          });
        }
        // 根据用户数组获取用户名称
        userModel.getTeamNames(uidArray, function(err, users){
          if(err){
            // 出现错误
            res.end(JSON.stringify({code: 0, msg: '获取用户信息时出错'}));
          }else{
            // 对DATA进行处理
            for(var d in data){
              for(var u in users){
                if(data[d].author == users[u].uid){
                  data[d].author = users[u];
                  break;
                }
              }
            }
            res.end(JSON.stringify({code: 1, msg: ';)', content: data}));
          }
        });
      }
    }
  );
});

// 发布动态
router.post('/sendDynamic', function(req, res){
  var content = req.body.content;                   // 内容
  var type    = parseInt(req.body.type);            // 动态类型|1、普通
  var sid     = req.session.uid;                    // 当前会话用户ID
  if(!sid){
    res.end(JSON.stringify({code: -10, msg: '您还没有登录，请先登录！'}));
    return;
  }
  // 检测数据
  if(!(content && content.length)){
    // 数据不合法
    res.end(JSON.stringify({code: 0, msg: '内容不能为空'}));
    return;
  }
  if(!type){
    res.end(JSON.stringify({code: 0, msg: '非法操作'}));
    return;
  }
  switch(type){
    case 1: // 普通动态|普通动态没有多余的字段需要验证
      dynamicModel.sendNormalDynamic(sid, content, function(err, dynamic){
        if(dynamic && dynamic.id){
          // 对数据进行处理
          var _dynamic = {
            id:       dynamic.id,
            author:   dynamic.author,
            content:  dynamic.content,
            praise:   dynamic.praise,
            comments: dynamic.comments,
            date:     dynamic.date
          }
          res.end(JSON.stringify({code: 1, msg: '发布成功', content: _dynamic}));
        }else{
          res.end(JSON.stringify({code: 0, msg: '发布失败，未知错误'}));
        }
      });
      break;
  }
});

module.exports = router;