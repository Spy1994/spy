/**
 * 用户模块
 * @author Abel i@iue.me
 */

var router       = require('express').Router();
var validator    = require('validator');
var userModel    = require('../../model/user');
var centerModel  = require('../../model/center');


// 获取用户基础信息
router.get('/baseInfo', function(req, res){
  var uid = req.query.uid || req.session.uid;
  if(!uid){
    res.end(JSON.stringify({code: 0, msg: '参数不正确'}));
    return;
  }
  userModel.findOne({uid: uid}, function(err, user){
    if(user && user.uid && user.state){
      // 判断当前用户是否在这个用户的黑名单中
      var suid = req.session.uid || 0;
      if(suid && user.black.indexOf(suid) >= 0){
        res.end(JSON.stringify({code: 0, msg: '很抱歉，您没有权限进行此操作！'}));
        return;
      }else{
        // 判断关系
        var relation = 0;
        if(suid == user.uid) relation = 9;
        if(user.fans.indexOf(suid) >= 0) relation = 1;
        if(user.idol.indexOf(suid) >= 0) relation = 2;
        if(user.fans.indexOf(suid) >= 0 && user.idol.indexOf(suid) >= 0) relation = 3;
        // 准备数据
        var userData = {
          uid: user.uid,
          username: user.name,
          email: user.mail,
          nickname: user.nickname || user.name,
          maxim: user.maxim || '混迹在边缘',
          area: user.area,
          fans: user.fans.length,
          idol: user.idol.length,
          sex: user.sex,
          age: user.age,
          school: user.school || [],
          balance: user.balance,
          relation: relation,
          birthday: user.birthday,
          star: user.constellation,
          grade: library.getPoints(user.points),
          skin: user.skin || library.getDefaultSkin()
        };
        res.end(JSON.stringify({code: 1, msg: userData}));
      }
    }else{
      res.end(JSON.stringify({code: 0, msg: '很抱歉，用户不存在或用户已被禁止使用！'}));
    }
  });
});

// 用户名是否存在
router.post('/usernameExist', function(req, res){
  userModel.findOne({name: req.body.username}, {_id:false, uid:true}, function(err, doc){
    res.end(JSON.stringify({exist: !!(doc && doc.uid)}));
  });
});

// 邮箱是否存在
router.post('/emailExist', function(req, res){
  userModel.findOne({mail: req.body.email}, {_id:false, uid:true}, function(err, doc){
    res.end(JSON.stringify({exist: !!(doc && doc.uid)}));
  });
});

// 登录验证
router.post('/login', function(req, res){
  var username = validator.trim(req.body.username);
  var password = library.encrypt(validator.trim(req.body.password));
  if(!(username && password)){
    res.end(JSON.stringify({code: 0, msg: '用户名或密码不能为空'}));
    return;
  }
  // 支持多种用户名登录：UID、用户名、邮箱
  var where = {};
  if(parseInt(username) == username)   where = {uid: username};
  else if(validator.isEmail(username)) where = {mail: username};
  else where = {name: username};
  where.pass  = password;
  userModel.findOne(where, {_id:false, uid:true, nickname:true, name: true}, function(err, user){
    if(user && user.uid){
      var state = parseInt(user.state);
      // 检测用户状态
      if(state === 0){
        res.end(JSON.stringify({code: 0, msg: '用户状态异常，无法登录!'}));
      }else if(state === -1){
        res.end(JSON.stringify({code: 0, msg: '管理员禁止了您账号，无法登录！'}));
      }else{
        // 验证通过
        token = userModel.updateSession(user.uid, req, res);
        var nickname = user.nickname || user.name;
        // 删除已有会话
        req.session.uid = 0;
        res.end(JSON.stringify({code: 1, msg: nickname+'，欢迎回来!'}));
      }
    }else{
      res.end(JSON.stringify({code: 0, msg: '用户名或密码有误！'}));
    }
  });
});

// 注册模块
router.post('/register', function(req, res){
  var username = validator.trim(req.body.username);
  var password = validator.trim(req.body.password);
  var email    = validator.trim(req.body.email);
  // 验证用户名
  if(!validator.isUsername(username)){
    res.end(JSON.stringify({code: -1, msg: '用户名无效'}));
    return;
  }
  // 验证密码
  if(!validator.isPassword(password)){
    res.end(JSON.stringify({code: -2, msg: '密码无效'}));
    return;
  }
  // 验证邮箱
  if(!validator.isEmail(email)){
    res.end(JSON.stringify({code: -3, msg: '邮箱地址无效'}));
    return;
  }
  // 验证用户名是否已存在
  userModel.findOne({name: username}, {uid: true, _id: false}, function(err, user){
    if(user && user.uid){
      // 用户名已存在
      res.end(JSON.stringify({code: -1, msg: '用户名已存在'}));
      return;
    }
    userModel.findOne({mail: email}, {uid: true, _id: false}, function(err, user){
      if(user && user.uid){
        // 邮箱已存在
        res.end(JSON.stringify({code: -3, msg: '邮箱已存在'}));
        return;
      }
      // 准备register数据
      var ip    = library.getClientIP(req);                           // 获取客户端IP地址
      var agent = req.get('User-Agent');                              // 获取客户端环境信息
      var registerData = {ip: ip, date: Date.now(), agent: agent};
      // 开始注册
      userModel.create({name: username, mail: email, pass: library.encrypt(password), register: registerData}, 
        function(err, user){
          if(user && user.uid){
            userModel.updateSession(user.uid, req, res);
            // 初始化扩展表
            centerModel.init(user.uid);
            // 销毁已有会话
            req.session.uid = 0;
            res.end(JSON.stringify({code: 1, msg: user.name + '，欢迎加入！'}));
          }else{
            console.log(err);
            res.end(JSON.stringify({code: 1, msg: '注册失败，未知错误，请重试！'}));
          }
        }
      );
    });
  });
});

module.exports = router;