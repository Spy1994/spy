/**
 * 用户模板
 * @author Abel i@iue.me
 */

var router = require('express').Router();


// 用户框架
router.get('/layout', function(req, res){
  res.render('user/layout');
});

// 用户主页
router.get('/index', function(req, res){
  res.render('user/index');
});

// 注册界面
router.get('/register', function(req, res){
  res.render('user/register');
});

// 动态页面
router.get('/dynamic', function(req, res){
  res.render('user/dynamic');
});

module.exports = router;