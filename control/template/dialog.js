/**
 * 弹窗
 * @author Abel i@iue.me
 */

var router = require('express').Router();

router.get('/login', function(req, res){
  res.render('dialog/login');
});

module.exports = router;